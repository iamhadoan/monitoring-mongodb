# Triển khai Prometheus giám sát mongoDB trên Kubernetes

## Triển khai Prometheus

### Cập nhật repository
```
$ helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

$ helm repo add stable https://kubernetes-charts.storage.googleapis.com/

$ helm repo update
```

### Cài đặt Prometheus
```
$ helm install prometheus prometheus-community/kube-prometheus-stack
```

### Một số command cơ bản

Lấy danh sách servicemonitor
```
$ kubectl get servicemonitor
```

Xem chi tiết thông tin, xuất ra theo định dạng YAML
```
$ kubectl get servicemonitor prometheus-kube-prometheus-grafana -oyaml

$ kubectl get prometheuses.monitoring.coreos.com -oyaml
```

## Triển khai mongoDB và mongoDB Exporter

### mongoDB
```
$ kubectl apply -f mongodb.yml
```

### mongoDB Exporter

#### Cập nhật repository
```
$ helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

$ helm repo update
```

#### Lấy values của exporter
```
$ helm show values prometheus-community/prometheus-mongodb-exporter
```

#### Cài đặt
```
$ helm install mongodb-exporter prometheus-community/prometheus-mongodb-exporter -f values.yml
```

#### Mở port mongoDB Exporter service
```
$ kubectl port-forward service/mongodb-exporter-prometheus-mongodb-exporter 9216
```

## Grafana

### Mở port
```
$ kubectl port-forward deployment/prometheus-grafana 3000
```
